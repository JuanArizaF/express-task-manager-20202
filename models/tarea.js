const mongoose = require("mongoose");

const TareaSchema = mongoose.Schema({
    titulo: {
        type:String,
        requiere: true
    },
    descripcion: String,
    fecha: {
        type: String,
        default:Date.now
    },
    terminada: {
        type:Boolean,
        default:false
    }, 
});

module.exports= mongoose.model('Tarea',TareaSchema)
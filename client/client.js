console.log("hello from client")

const axios = require("axios");

async function crearTarea() {
    try {
        let respuesta = await axios.post("http://localhost:3000/tareas", { titulo: "Take a pictures" , description:"Relajadito"})
        console.log(respuesta.data);
    } catch (error) {
        console.log(respuesta)
    }
}
// crearTarea();

async function obtenerTodasLasTareas(){
    try {
        let tareas= await axios.get("http://localhost:3000/tareas/")
        console.log(tareas.data)
    } catch (error) {
        console.log(error)
    }
}
// obtenerTodasLasTareas();

async function obtenerTarea(id){
    try {
        let tareas= await axios.get("http://localhost:3000/tareas/"+id)
        console.log(tareas.data)
    } catch (error) {
        console.log(error)
    }
}
//obtenerTarea("5fa40f4b7f058342b87606d7");
async function modificarTarea(id){
    try {
        let respuesta= await axios.patch("http://localhost:3000/tareas/"+id,{titulo:"Wacht tv"});
        console.log(respuesta.data)
    } catch (error) {
        console.log(error)
    }
}
//modificarTarea("5fa40f4b7f058342b87606d7")
async function eliminarTarea(id){
    try {
        let respuesta= await axios.delete("http://localhost:3000/tareas/"+id);
        console.log(respuesta.data)
    } catch (error) {
        console.log(error)
    }
}
eliminarTarea("5fa40f4b7f058342b87606d7")
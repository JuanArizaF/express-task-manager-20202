const express = require("express");
const mongoose = require("mongoose");
const tareasRoute =require('./routes/tareas');
const bodyParser = require("body-parser");
const cors = require("cors");

require('dotenv/config')

const app=express();
app.use(cors())
app.use(bodyParser.json())

app.use('/tareas', tareasRoute)

app.get("/",(req, res) =>{
    res.send("Hola Express");
});

//mongodb+srv://juan:<password> @cluster0.gynmq.mongodb.net/tareas?retryWrites=true&w=majority
	
mongoose.connect(process.env.CONEXION_DB,
    
    { useUnifiedTopology: true, useNewUrlParser: true },
    () => {
    //   console.log("Conectado a la base de datos...");
    }
  );

app.listen(3000);
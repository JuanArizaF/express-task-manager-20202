const express = require('express');
const tarea = require('../models/tarea');
const router = express.Router();
const Tarea = require('../models/tarea')
router.get("/", async(req, res) =>{
    //res.send("Hola mi so tareas");
    try {
        const tareas= await Tarea.find();
        res.json(tareas)
    } catch (error) {
        res.status(500).send(error)
    }
});
// router.get("/1",(req, res) =>{
//     res.send("Hola mi so tareas esta es la 1");
// });

router.get('/:id',async (req,res)=>{
    let id= req.params.id;
    console.log("el indentificador es" + id);
    try {
        const tarea =await Tarea.findById(id);
        res.json(tarea)
    } catch (error) {
        res.status(500).send(error)
    }
    res.send("obteniedo tarea")
})

router.delete('/:id',async (req,res)=>{
    let id= req.params.id;
    console.log("el indentificador es" + id);
    try {
        const resultado= await Tarea.remove({_id:id})
        res.json(resultado)
    } catch (error) {
        res.status(500).send(error)
    }
    res.send("obteniedo tarea")
})

router.post("/",async(req, res) =>{
    console.log("llego al post de tareas");
    
    try{
        const nuevaTarea = new Tarea({
            titulo: req.body.titulo
        })
    
        let resultado= await nuevaTarea.save();
    
        // res.send("Creando una tarea...");
        res.json(resultado);
    }catch (error){
        res.status(500).send(error);

    }
    
});
 router.patch("/:id",async (req,res)=>{
     let id =req.params.id
     try {
        const resultados =await Tarea.updateOne(
            {_id:id},
            {
                $set:req.body,
            }
        )
        return res.json(resultados)
       //  res.json({message:'el id recivido es '+ id})
     } catch (error) {
         return res.status(500).send(error)
     }
 })
module.exports = router;